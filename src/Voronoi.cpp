#include "Voronoi.h"

Voronoi::Voronoi()
{
    //ctor
}

Voronoi::~Voronoi()
{
    //dtor
}
void Voronoi::Make(int width, int height, vector<Point> points, vector<Point> allpoints)
{
    double moveX = mapX_0(allpoints,width);
    double moveY = mapY_0(allpoints,height);

    for(int i =0; i < points.size(); i++){
        points[i].x += moveX;
        points[i].y += moveX;
    }
    for(int i =0; i < allpoints.size(); i++){
        allpoints[i].x += moveX;
        allpoints[i].y += moveY;
    }

    double multiplyX = mapX(allpoints,width);
    double multiplyY = mapY(allpoints,height);

    for(int i =0; i < points.size(); i++){
        points[i].x *= multiplyX;
        points[i].y *= multiplyY;
    }
    for(int i =0; i < allpoints.size(); i++){
        allpoints[i].x *= multiplyX;
        allpoints[i].y *= multiplyY;
    }

    bmp_ = new MyBitmap();
    bmp_->Create(width,height);
    bmp_->SetPenColor(0);
    points_ = points;
    Allpoints_ = allpoints;
    CreateColors();
    CreateSites();
    SetSitesPoints();
    bmp_->SaveBitmap("v.bmp");
}

void Voronoi::CreateSites() {
    int w = bmp_->width(), h = bmp_->height(), d;
    for (int hh = 0; hh < h; hh++) {
        for (int ww = 0; ww < w; ww++) {
            int ind = -1, dist = INT_MAX;
            for (size_t it = 0; it < points_.size(); it++) {
                const Point& p = points_[it];
                d = DistanceSqrd(p, ww, hh);
                if (d < dist) {
                    dist = d;
                    ind = it;
                }
            }
            if (ind > -1){
            SetPixel(bmp_->hdc(), ww, hh, colors_[ind]);
            }
        }
    }
}
//marks point on diagram
void Voronoi::SetSitesPoints()
{

    for (const auto& point : Allpoints_) {
        int x = point.x, y = point.y;
        for (int i = -1; i < 2; i++){
            for (int j = -1; j < 2; j++){
                SetPixel(bmp_->hdc(), x + i, y + j, 255);
            }
        }
    }
    for (const auto& point : points_) {
        int x = point.x;
        int y = point.y;
        if (x == 0){
            x = 1;
        }
        if (y == 0){
            y = 1;
        }
        for (int i = -1; i < 2; i++){
            for (int j = -1; j < 2; j++){
                SetPixel(bmp_->hdc(), x + i, y + j, 0);
            }
        }
    }
}
//color generator for diagram
void Voronoi::CreateColors() {
    for (size_t i = 0; i < points_.size(); i++) {
      DWORD c = RGB(rand() % 200 + 50, rand() % 200 + 55, rand() % 200 + 50);
      colors_.push_back(c);
    }
}

int Voronoi::DistanceSqrd(const Point& point, int x, int y) {
    int xd = x - point.x;
    int yd = y - point.y;
    return (xd * xd) + (yd * yd);
}
double Voronoi::mapX(vector<Point> points,int width)
{
    double x = points[0].x;
    for(int i = 0; i < points.size(); i++){
        if(x < points[i].x){
            x = points[i].x;
        }
    }
    return (double)width / x;
}
double Voronoi::mapY(vector<Point> points,int height)
{
    double y = points[0].y;
    for(int i = 0; i < points.size(); i++){
        if(y < points[i].y){
            y = points[i].y;
        }
    }
    return (double)height / y;
}
double Voronoi::mapX_0(vector<Point> points,int width)
{
    double x = 0;
    for(int i = 0; i < points.size(); i++){
        double test = points[i].x;
        if(x > points[i].x){
            x = points[i].x;
        }
    }
    return abs(x);
}
double Voronoi::mapY_0(vector<Point> points,int height)
{
    double y = 0;
    for(int i = 0; i < points.size(); i++){
           double test = points[i].y;
        if(y > points[i].y){
            y = points[i].y;
        }
    }
    return abs(y);
}
