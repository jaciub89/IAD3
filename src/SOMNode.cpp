#include "SOMNode.h"

SOMNode::SOMNode(double x, double y, int NumWeights)
{
    this->x = x;
    this->y = y;
    for (int w=0; w<NumWeights; ++w)
    {
      m_dWeights.push_back(random());
    }
}

SOMNode::~SOMNode()
{
    //dtor
}
double SOMNode::CalculateDistance(const vector<double> &InputVector)
{
  double distance = 0;

  for (int i=0; i<m_dWeights.size(); ++i)
  {
    distance += (InputVector[i] - m_dWeights[i]) *
                (InputVector[i] - m_dWeights[i]);
  }

  return distance;
}
void SOMNode::AdjustWeights(const vector<double> &target,
                          const double LearningRate,
                          const double Influence)
{
  for (int w=0; w<target.size(); ++w)
  {
    m_dWeights[w] += LearningRate * Influence * (target[w] - m_dWeights[w]);
  }
}
double SOMNode::X()
{
    return x;
}
double SOMNode::Y()
{
    return y;
}
double SOMNode::random(){
    int n = rand() % 1000 - 500;

    return n / 100.0f;
}
