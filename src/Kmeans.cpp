#include "Kmeans.h"

Kmeans::Kmeans(int nrofclusters, vector<Point> input)
{
    clusterNr = nrofclusters;
    this->input = input;

    //init cluster count
    clusterCount.resize(nrofclusters);
    for(int i = 0; i < nrofclusters; i++){
        clusterCount.at(i) = 0;
    }
    //generate first random centroids
    centroids = generateCentroids();

    oldCentroids.resize(nrofclusters);
    master.resize(nrofclusters);
    distance.resize(nrofclusters);

}
Kmeans::~Kmeans()
{
    //dtor
}
vector<Point> Kmeans::generateCentroids()
{
    srand(time(NULL));
    vector<Point>result;
    vector<int>alreadyVisited;
    int i = 0;
    result.resize(clusterNr);
    do{
        //get some random number to input to centroid
        int number = rand()%input.size();
        //check if we already have this point as centroid
        if (!( find(alreadyVisited.begin(), alreadyVisited.end(), number) != alreadyVisited.end())){
            result.at(i) = input[number];
            alreadyVisited.push_back(number);
            i++;
        }
    }while(i < clusterNr);
    return result;
}
double Kmeans::distanceP(Point p1, Point p2)
{
    return sqrt(pow(p1.x - p2.x,2) + pow(p1.y - p2.y,2));
}
int Kmeans::pos()
{
    double dis = distance.at(0);
    int result = 0;
    for(int i = 0; i < clusterNr;i++){
        if( dis > distance.at(i)){
            dis = distance.at(i);
            result = i;
        }
    }
    return result;
}
bool Kmeans::nextIteration()
{
    bool result = false;
    for(int i =0; i < clusterNr;i++){
        if(centroids[i].x != oldCentroids[i].x){
                return result = true;
        }
        if(centroids[i].y != oldCentroids[i].y){
                return result = true;
        }
    }
    return result;
}
void Kmeans::printResult(int width, int height)
{
    vector<Point> forVoronoiMaster;
    Voronoi w;
    double totalError = 0;
    for(int i =0; i < clusterNr; i++){
        double error = errorRate(centroids[i],master.at(i));
        cout<<"\nCentroid :"<<i+1<<".\t("<<centroids[i].x<<";"<<centroids[i].y<<")\n";
        cout<<"\nBlad kwantyzacji :"<<error;
        totalError += error;
        for(int j = 0; j < master.at(i).size(); j++){
            forVoronoiMaster.push_back(master[i][j]);
        }
    }
    cout<<"\nSredni blad kwantyzacji: "<<totalError / clusterNr<<"\n";
    w.Make(width,height,centroids,forVoronoiMaster);
}
void Kmeans::start()
{
    Point tmp;
    do{
        //clear cluster counters and cluster with specific points
        for(int i = 0; i <clusterNr;i++){
                master.at(i).clear();
                clusterCount.at(i) = 0;
        }
        //copy old centroids
        for(int i = 0; i < clusterNr ;i++){
            oldCentroids.at(i) = centroids[i];
        }
        //calculate distance between point and centroid
        for(int i=0; i < input.size();i++){
            for(int j=0; j < clusterNr; j++){
                distance.at(j) = distanceP(centroids[j],input[i]);
            }
            //check to which cluster add point and push it
            int position = pos();

            master.at(position).push_back(input[i]);
            //increment cluster counter
            clusterCount.at(position) += 1;
        }

        for(int i = 0; i < clusterNr ;i++){
        //calculate median point for each cluster
        tmp.x = 0;
        tmp.y = 0;
            if(master.at(i).size()>0){
                //calculate for cluster i
                for(int j=0; j <master.at(i).size();j++){
                    tmp.x += master[i][j].x;
                    tmp.y += master[i][j].y;
                }
                tmp.x = tmp.x / clusterCount.at(i);
                tmp.y = tmp.y / clusterCount.at(i);
            }
            else{
                //if there are no point in cluster rewrite the centroid
                tmp = centroids.at(i);
            }
            centroids.at(i) = tmp;
        }
    }while(nextIteration());
}
double Kmeans::errorRate(Point centroid, vector<Point> source)
{
    double dSum = 0;
    for(int i =0; i< source.size();i++){
        dSum += (pow((source[i].x - centroid.x),2) + pow((source[i].y - centroid.y),2));
    }
    return dSum / source.size();
}
