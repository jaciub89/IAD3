#ifndef SOMNODE_H
#define SOMNODE_H
#include <vector>
#include <cstdlib>
#include "Point.h"

#include <iostream>
#include <cstdio>

using namespace std;

class SOMNode
{
    public:
        SOMNode(double x, double y, int NumWeights);
        virtual ~SOMNode();
        double CalculateDistance(const vector<double> &InputVector);
        void AdjustWeights(const vector<double> &vec,const double LearningRate, const double Influence);
        double X();
        double Y();
    private:
        vector<double> m_dWeights;
        double x;
        double y;
        double random();
};

#endif // SOMNODE_H
