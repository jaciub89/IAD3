#ifndef KMEANS_H
#define KMEANS_H
#include <vector>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <iostream>
#include <ctime>
#include "Voronoi.h"

using namespace std;

class Kmeans
{
    public:
        Kmeans(int nrofclusters, vector<Point> input);
        virtual ~Kmeans();
        void start();
        void printResult(int width, int height);
    private:
        int clusterNr;
        vector<Point> input;
        vector<vector<Point>> master;
        vector<Point> centroids;
        vector<Point> oldCentroids;
        vector<int> clusterCount;
        vector<Point> generateCentroids();
        vector<double> distance;
        double errorRate(Point centroid, vector<Point> source);
        double distanceP(Point p1, Point p2);
        int pos();
        bool nextIteration();

};

#endif // KMEANS_H
