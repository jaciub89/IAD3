#ifndef MYBITMAP_H
#define MYBITMAP_H
#define _WIN32_WINNT 0x0500
#include <windows.h>

class MyBitmap {
 public:
  MyBitmap();
  ~MyBitmap();
  bool Create(int w, int h);

  void SetPenColor(DWORD clr);
  bool SaveBitmap(const char* path);
  HDC hdc();
  int width();
  int height();

 private:
  HBITMAP bmp_;
  HDC hdc_;
  HPEN pen_;
  int width_;
  int height_;
};

#endif // MYBITMAP_H
