#ifndef VORONOI_H
#define VORONOI_H
#include <vector>
#include <climits>
#include "MyBitmap.h"
#include "Point.h"

using namespace std;

class Voronoi {
 public:
  void Make(int width, int height, vector<Point> points, vector<Point> allpoints);
  Voronoi();
  ~Voronoi();
 private:
  void CreateSites();
  void SetSitesPoints();
  void CreateColors();
  int DistanceSqrd(const Point& point, int x, int y);
  vector<Point> points_;
  vector<Point> Allpoints_;
  vector<DWORD> colors_;
  MyBitmap* bmp_;
  double mapX(vector<Point> points,int width);
  double mapY(vector<Point> points,int height);
  double mapY_0(vector<Point> points,int height);
  double mapX_0(vector<Point> points,int height);
};

#endif // VORONOI_H
