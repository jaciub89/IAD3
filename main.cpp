#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <fstream>
#include "Voronoi.h"
#include "Kmeans.h"
vector<Point> loadPoints(string path,int columns, char sign){
    fstream file;
    file.open(path.c_str());
    vector<Point>result;
    string data,tmp,type;
    if(file.is_open()){
        while(getline(file,data,sign)){
                Point next;
                next.x = atof(data.c_str());
                for(int i = 1; i < columns; i++){
                    if (i == (columns-1)){
                        getline(file, tmp);
                    }
                    else{
                        getline(file, tmp, sign);
                    }
                    next.y = atof(tmp.c_str());
                }
                result.push_back(next);
        }
        file.close();
    }
    else{
        cout<<"error - blad otwarcia: " + path + "\n";
    }
    return result;
}
int main(){

    Voronoi w;
    int kNumbers;
    vector<Point>_points = loadPoints("points.txt",2,',');
    cout<<"\nNa ile przedzialow podzielic ?\n";
    cin>>kNumbers;

    Kmeans k(kNumbers,_points);
    k.start();
    k.printResult(1024,1024);


    return 0;
};

